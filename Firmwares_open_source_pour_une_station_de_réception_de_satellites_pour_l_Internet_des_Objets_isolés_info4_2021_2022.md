# Journal de bord - Firmwares open source pour une station de réception de satellites pour l’Internet des Objets isolés

## CARMONA Damian, DA COSTA Tom, WOZNY Pierre-Raphael

### 24/01/2022

Récupération du matériel au Fablab :

Support TinyGS x 3

ESP32 x3

Module Lambda80 x3

Antenne 2.4 Ghz connecteur UFL x3

cables micro-USB x3

module GPS Grove x1

Installation de riotOS et suivi des tutos sur : https://github.com/riot-os/riot-course

### 31/01/2022

Réunion avec Didier Donsez, explication des objectifs.

Récupération des modules manquant, vérification du fonctionnement des cartes et installation de Arduino IDE (1.8.19).
Getting started :

https://electropeak.com/learn/getting-started-with-the-esp32/ 

https://docs.espressif.com/projects/arduino-esp32/en/latest/installing.html

https://github.com/espressif/arduino-esp32

Test du firmware MQTT, envoie de message sur le réseau via un serveur mosquito et réception du message sur les cartes connectées au réseau
Remarque : les cartes non pas de LED utilisable (autre que la LED de boot)


### 07/02/2022

#### 14h30 réunion zoom :

Au programme: coordination Ecole de l’Air et de l’Espace et UGA CSUG Polytech

Contact :  Antoine Ciborski, Leïla Bachiri
leila.bachiri@salon-air.fr 
leilaera123@gmail.com

On parlera de https://wiki.satnogs.org/SuperAntennaz 

Didier DONSEZ vous invite à une réunion Zoom planifiée.

Sujet : Point TinyGS 2G4
Heure : 7 févr. 2022 02:30 PM Bruxelles

Participer à la réunion Zoom
https://univ-grenoble-alpes-fr.zoom.us/j/95886821244?pwd=QzFlNlJEZzkrQm9wTEx4Z3ZLNGxGZz09 

ID de réunion : 958 8682 1244
Code secret : 436885
Une seule touche sur l’appareil mobile
+12532158782,,95886821244# États-Unis (Tacoma)
+13017158592,,95886821244# États-Unis (Washington DC)

Composez un numéro en fonction de votre emplacement
        +1 253 215 8782 États-Unis (Tacoma)
        +1 301 715 8592 États-Unis (Washington DC)
        +1 312 626 6799 États-Unis (Chicago)
        +1 346 248 7799 États-Unis (Houston)
        +1 646 558 8656 États-Unis (New York)
        +1 669 900 9128 États-Unis (San Jose)
ID de réunion : 958 8682 1244
Trouvez votre numéro local : https://univ-grenoble-alpes-fr.zoom.us/u/ad6wrC4prc

Participer à l’aide d’un protocole SIP
95886821244@zoomcrc.com

Participer à l’aide d’un protocole H.323
162.255.37.11 (États-Unis (Ouest))
162.255.36.11 (États-Unis (Est))
213.19.144.110 (Amsterdam Pays-Bas)
213.244.140.110 (Allemagne)
Code secret : 436885
ID de réunion : 958 8682 1244

#### Tests Firmware :

Tests de la librairie SX1280 et mise en place d'un transmetteur et receveur 2.4g tinyGS Lora
Cartes opérationnelles, reçoivent toutes un message et sont capables d'en transmettre un.

Problème rencontré :

Un des modules esp32 avait ses broches dans le vide, mal connecté.
Problème de permission du port sur Archlinux (manjaro) nécessite la commande sudo chmod +666 [nom d'utilisateur] /dev/ttyUSB0

Résultat : 

![alt text](https://i.imgur.com/e1XTZrS.png)

### 14/02/2022 :

14h40 réunion de suivi zoom avec Didier Donsez 

Travail réalisé :

Tentative d'utilisation de la librairie AioP13 pour le calcul et la prédiction de TLE.

Problème rencontré :

Manque de documentation sur la librairie.

### 27/02/2022

Travail réalisé :

Installation du firmware tinyGS sur une des cartes et test de la configuration de la station afin de la faire apparaitre sur le réseau tinyGS.
Accès au Télégram de la communauté TinyGS.

### 07/03/2022

Travail réalisé :

Configuration du firmware TinyGS sur deux cartes avec la version beta.
Accès à la template du module SX1280 2.4 GHz grâce à la version beta. Ce qui n'est pas possible avec la version du master.
Renseignements sur les opérations à effectuer pour tester la réception/émission de la carte.

### 14/03/2022 et 15/03/2022

Récupération d'une carte heltec 868 Mhz au Fablab

Travail réalisé :

Compréhension et debug du code du firmware TinyGS.
Mise en place de test de réception/émission d'une station en immitant un satellite émettant avec l'autre station.
Possibilité de recevoir de les messages MQTT cf image 
![Test réception de la station](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/21-22/02/docs/-/raw/main/test_envoie_msg_tiny_gs.png)
Problème rencontré :

Contenu du message non visible
Pas encore capable de recevoir les messages envoyés par la station.

### 21/03/2022 et 22/03/2022
15h00 réunion de suivi avec Didier Donsez

Travail réalisé :

Compréhension et inspection du code du firmware TinyGS, configuration des paramètres du board via la configuration manuelle. Test de la configuration automatique. Vérification du config manager du Firmware.

Problème rencontré :

La carte n'arrive pas à recevoir des messages car le board n'est pas correctement configuré.

### 28/03/2022 et 29/03/2022

Réunion de suivi avec Didier Donsez

Travail réalisé : Recherche sur les raisons du problème de configuration du board. Problème lier à la gestion du module sx1280 et son support par le firmware. Le code semble le gérer mais la doc ne l'indique pas. Ajout de log dans le code.

Problème rencontré : Même problème de configuration du board.

Travail à faire : comprendre le problème et réussir à configurer la station correctement.
